#ifndef _FPS_H
#define _FPS_H

// From this gist: https://gist.github.com/kenpower/7233967
class FPS
{
public:
    FPS() : mFrame(0), mFps(0) {}
    const unsigned int getFPS() const
    {
        return mFps;
    }

private:
    unsigned int mFrame;
    unsigned int mFps;
    sf::Clock mClock;

public:
    void update()
    {
        if(mClock.getElapsedTime().asSeconds() >= 1.f)
        {
            mFps = mFrame;
            mFrame = 0;
            mClock.restart();
        }

        ++mFrame;
    }
};

#endif
