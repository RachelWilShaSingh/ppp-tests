#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>

template <typename T>
std::string ToString( const T& value )
{
    std::stringstream ss;
    ss << value;
    return ss.str();
}

// From this gist: https://gist.github.com/kenpower/7233967
class FPS
{
public:
    FPS() : mFrame(0), mFps(0) {}
    const unsigned int getFPS() const
    {
        return mFps;
    }

private:
    unsigned int mFrame;
    unsigned int mFps;
    sf::Clock mClock;

public:
    void update()
    {
        if(mClock.getElapsedTime().asSeconds() >= 1.f)
        {
            mFps = mFrame;
            mFrame = 0;
            mClock.restart();
        }

        ++mFrame;
    }
};

int main()
{
    srand( time( NULL ) );
    int circleCount;
    std::cout << "How many sprites? ";
    std::cin >> circleCount;

    sf::RenderWindow app( sf::VideoMode( 800, 800 ), "Gyro test", sf::Style::Fullscreen );
    app.setFramerateLimit( 60 );
    sf::Vector2f windowDimensions = sf::Vector2f( sf::VideoMode::getDesktopMode().width,
                                    sf::VideoMode::getDesktopMode().height );


    FPS fps;

    sf::RectangleShape background;
    background.setFillColor( sf::Color( 100, 100, 100 ) );
    background.setPosition( 0, 0 );
    background.setSize( windowDimensions );

    sf::Texture texture;
    texture.loadFromFile( "../assets/ayda-single.png" );

    std::vector<sf::Sprite> sprites;
    std::vector<sf::Vector2f> directional;
    for ( int i = 0; i < circleCount; i++ )
    {
        sf::Sprite object;
        object.setTexture( texture );
        object.setPosition( rand() % int( windowDimensions.x ), rand() % int( windowDimensions.y ) );
        sprites.push_back( object );

        sf::Vector2f direction;
        direction.x = rand() % 2;
        direction.x = ( direction.x == 0 ) ? -1 : 1;
        direction.y = rand() % 2;
        direction.y = ( direction.y == 0 ) ? -1 : 1;
        direction.x *= rand() % 5 + 1;
        direction.y *= rand() % 5 + 1;
        directional.push_back( direction );
    }

    sf::Font fntMain;
    fntMain.loadFromFile( "../assets/mononoki-Bold.ttf" );

    std::vector<sf::Text> texts;

    sf::Text txt;
    txt.setFont( fntMain );
    txt.setCharacterSize( 20 );
    txt.setFillColor( sf::Color::White );
    int x = 5, y = 5, inc = 20;
    txt.setPosition( x, y );
    y += inc;
    txt.setString( "Screen dimensions: " + ToString( windowDimensions.x ) + ", " + ToString( windowDimensions.y ) );
    texts.push_back( txt );
    txt.setPosition( x, y );
    y += inc;
    txt.setString( "FPS: xyz" );
    texts.push_back( txt );

    while ( app.isOpen() )
    {
        fps.update();

        sf::Event event;
        while ( app.pollEvent( event ) )
        {
            if ( event.type == sf::Event::Closed )
                app.close();
        }

        for ( size_t i = 0; i < sprites.size(); i++ )
        {
            sf::Vector2f pos = sprites[i].getPosition();
            pos.x += directional[i].x;
            pos.y += directional[i].y;
            sprites[i].setPosition( pos );

            // At edge?
            if ( pos.x < 0 || pos.x > windowDimensions.x )
            {
                directional[i].x = -directional[i].x;
            }
            if ( pos.y < 0 || pos.y > windowDimensions.y )
            {
                directional[i].y = -directional[i].y;
            }
        }

        texts[1].setString( "FPS: " + ToString( fps.getFPS() ) );

        app.clear();

        app.draw( background );

        for ( auto& obj : sprites )
        {
            app.draw( obj );
        }

        for ( auto& text : texts )
        {
            app.draw( text );
        }

        app.display();
    }

    return EXIT_SUCCESS;
}
