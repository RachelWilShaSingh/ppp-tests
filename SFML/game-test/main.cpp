#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <string>
#include <sstream>
#include <cmath>

template <typename T>
std::string ToString( const T& value )
{
    std::stringstream ss;
    ss << value;
    return ss.str();
}

void SetGoal( sf::Vector2f toLocation, sf::Vector2f& goal, sf::CircleShape& goalCircle, sf::Text& txtDebug  )
{
    goal = toLocation;
    goalCircle.setPosition( goal.x, goal.y );
    txtDebug.setString( "Move to: " + ToString( goal.x ) + ", " + ToString( goal.y ) );
}

sf::Vector2f RandomPosition( int maxX, int maxY )
{
    sf::Vector2f randomPos;
    randomPos.x = rand() % maxX;
    randomPos.y = rand() % maxY;
    return randomPos;
}

float Distance( const sf::Vector2f& center1, const sf::Vector2f& center2 )
{
    float xdiff = center2.x - center1.x;
    float ydiff = center2.y - center1.y;
    return sqrt( xdiff * xdiff + ydiff * ydiff );
}

int main()
{
    const int SCREEN_WIDTH = 400;
    const int SCREEN_HEIGHT = 760;

    sf::RenderWindow app( sf::VideoMode( SCREEN_WIDTH, SCREEN_HEIGHT ), "SFML + PinePhone Pro" );
    app.setFramerateLimit( 60 );

    sf::Texture txAyda;
    txAyda.loadFromFile( "../assets/ayda-single.png" );

    sf::Texture txStick;
    txStick.loadFromFile( "../assets/stick.png" );

    sf::Texture txGrass;
    txGrass.loadFromFile( "../assets/grass.png" );

    sf::Sprite ayda( txAyda );
    ayda.setPosition( SCREEN_WIDTH/2 - 16, SCREEN_HEIGHT/2 - 24 );
    ayda.setOrigin( 16, 24 );

    sf::Sprite stick( txStick );
    stick.setOrigin( 16, 16 );
    stick.setPosition( RandomPosition( SCREEN_WIDTH - 32, SCREEN_HEIGHT - 32 ) );

    sf::Sprite grass( txGrass );
    grass.setPosition( 0, 0 );

    sf::Font fntMain;
    fntMain.loadFromFile( "../assets/mononoki-Bold.ttf" );

    sf::Music music;
    music.openFromFile( "../assets/PiroIntro2.ogg" );
    music.setVolume( 50 );
    music.play();

    sf::SoundBuffer sbCollect;
    sbCollect.loadFromFile( "../assets/collect.ogg" );

    sf::Sound sfxCollect( sbCollect );
    sfxCollect.setVolume( 60 );

    sf::Text txtScore;
    txtScore.setFont( fntMain );
    txtScore.setCharacterSize( 24 );
    txtScore.setPosition( 10, 10 );
    txtScore.setFillColor( sf::Color::White );

    int score = 0;
    txtScore.setString( "Score: " + ToString( score ) );

    sf::Text txtDebug;
    txtDebug.setFont( fntMain );
    txtDebug.setCharacterSize( 20 );
    txtDebug.setPosition( 10, 30 );
    txtDebug.setFillColor( sf::Color::White );

    sf::Text txtDebug2;
    txtDebug2.setFont( fntMain );
    txtDebug2.setCharacterSize( 20 );
    txtDebug2.setPosition( 10, 50 );
    txtDebug2.setFillColor( sf::Color::White );
    txtDebug2.setString( "Distance: " + ToString( Distance( ayda.getPosition(), stick.getPosition() ) ) );

    sf::Vector2f goal;
    float speed = 5;
    sf::CircleShape goalCircle( 16 );
    goalCircle.setOrigin( 8, 8 );
    goalCircle.setFillColor( sf::Color( 255, 255, 255, 100 ) );

    SetGoal( ayda.getPosition(), goal, goalCircle, txtDebug );

    while (app.isOpen())
    {
        sf::Event event;
        while ( app.pollEvent( event ) )
        {
            if ( event.type == sf::Event::Closed )
                app.close();
        }

        // Check for mouse click
        if ( sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
        {
            SetGoal( sf::Vector2f( sf::Mouse::getPosition( app ).x, sf::Mouse::getPosition( app ).y ),
                    goal, goalCircle, txtDebug );
        }

        // Check for screen touch
        if ( sf::Touch::isDown( 0 ) )
        {
            SetGoal( sf::Vector2f( sf::Touch::getPosition( 0, app ).x, sf::Touch::getPosition( 0, app ).y ),
                    goal, goalCircle, txtDebug );
        }

        txtDebug2.setString( "Distance: " + ToString( Distance( ayda.getPosition(), stick.getPosition() ) ) );

        // Move player toward goal area
        sf::Vector2f playerPos = ayda.getPosition();
        if ( goal.x < playerPos.x - speed )
        {
            playerPos.x -= speed;
        }
        else if ( goal.x > playerPos.x  + speed)
        {
            playerPos.x += speed;
        }
        if ( goal.y < playerPos.y - speed )
        {
            playerPos.y -= speed;
        }
        else if ( goal.y > playerPos.y + speed )
        {
            playerPos.y += speed;
        }
        ayda.setPosition( playerPos );

        // Check for collision with stick
        if ( Distance( ayda.getPosition(), stick.getPosition() ) < 32 )
        {
            score += 1;
            txtScore.setString( "Score: " + ToString( score ) );
            stick.setPosition( RandomPosition( SCREEN_WIDTH - 32, SCREEN_HEIGHT - 32 ) );
            sfxCollect.play();
        }

        app.clear();

        app.draw( grass );

        // Draw circle where player wants to go
        app.draw( goalCircle );

        app.draw( stick );
        app.draw( ayda );

        app.draw( txtDebug );
        app.draw( txtDebug2 );
        app.draw( txtScore );

        app.display();
    }

    return EXIT_SUCCESS;
}
