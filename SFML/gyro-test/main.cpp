#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iostream>

template <typename T>
std::string ToString( const T& value )
{
    std::stringstream ss;
    ss << value;
    return ss.str();
}

int main()
{
    const int SCREEN_WIDTH = 400;
    const int SCREEN_HEIGHT = 760;

    sf::RenderWindow app( sf::VideoMode( SCREEN_WIDTH, SCREEN_HEIGHT ), "Gyro test", sf::Style::Fullscreen );
    app.setFramerateLimit( 60 );
    sf::Vector2f windowDimensions = sf::Vector2f( sf::VideoMode::getDesktopMode().width,
                                                 sf::VideoMode::getDesktopMode().height );

    sf::RectangleShape background;
    background.setFillColor( sf::Color( 100, 100, 100 ) );
    background.setPosition( 0, 0 );
    background.setSize( windowDimensions );

    sf::CircleShape object;
    object.setFillColor( sf::Color::Green );
    object.setRadius( 30 );
    object.setOrigin( 15, 15 );
    object.setPosition( windowDimensions.x / 2, windowDimensions.y / 2 );

    std::string gyro = "Gyroscope ";
    std::string gravity = "Gravity ";
    std::string accelerometer = "Accelerometer ";

    if ( sf::Sensor::isAvailable( sf::Sensor::Gyroscope ) )
    {
        sf::Sensor::setEnabled( sf::Sensor::Gyroscope, true );
        gyro += "enabled";
    }
    else
    {
        std::cout << "Gyroscope not available!" << std::endl;
        gyro += "disabled";
    }

    if ( sf::Sensor::isAvailable( sf::Sensor::Gravity ) )
    {
        sf::Sensor::setEnabled( sf::Sensor::Gravity, true );
        gravity += "enabled";
    }
    else
    {
        std::cout << "Gravity sensor not available!" << std::endl;
        gravity += "disabled";
    }

    if ( sf::Sensor::isAvailable( sf::Sensor::Accelerometer ) )
    {
        sf::Sensor::setEnabled( sf::Sensor::Accelerometer, true );
        accelerometer += "enabled";
    }
    else
    {
        std::cout << "Accelerometer not available!" << std::endl;
        accelerometer += "disabled";
    }

    sf::Font fntMain;
    fntMain.loadFromFile( "../assets/mononoki-Bold.ttf" );

    std::vector<sf::Text> texts;

    sf::Text txt;
    txt.setFont( fntMain );
    txt.setCharacterSize( 20 );
    txt.setFillColor( sf::Color::White );

    int x = 5, y = 5, inc = 20;

    txt.setPosition( x, y ); y += inc;
    txt.setString( "Screen dimensions: " + ToString( windowDimensions.x ) + ", " + ToString( windowDimensions.y ) );
    texts.push_back( txt );

    txt.setPosition( x, y ); y += inc;
    txt.setString( gyro );
    texts.push_back( txt );

    txt.setPosition( x, y ); y += inc;
    txt.setString( gravity );
    texts.push_back( txt );

    txt.setPosition( x, y ); y += inc;
    txt.setString( accelerometer );
    texts.push_back( txt );

    txt.setPosition( x, y ); y += inc;
    txt.setString( "Gyroscope:" );
    texts.push_back( txt );

    txt.setPosition( x, y ); y += inc;
    txt.setString( "Gravity:" );
    texts.push_back( txt );

    txt.setPosition( x, y ); y += inc;
    txt.setString( "Accelerometer:" );
    texts.push_back( txt );

    while ( app.isOpen() )
    {
        sf::Event event;
        while ( app.pollEvent( event ) )
        {
            if ( event.type == sf::Event::Closed )
                app.close();
        }

        sf::Vector3f vGyro = sf::Sensor::getValue( sf::Sensor::Gyroscope );
        sf::Vector3f vGrav = sf::Sensor::getValue( sf::Sensor::Gravity );
        sf::Vector3f vAcc = sf::Sensor::getValue( sf::Sensor::Accelerometer );

        texts[ texts.size() - 1 ].setString( "Accelerometer: "  + ToString( vAcc.x ) + ", " + ToString( vAcc.y ) + ", " + ToString( vAcc.z ) );
        texts[ texts.size() - 2 ].setString( "Gravity: "        + ToString( vGrav.x ) + ", " + ToString( vGrav.y ) + ", " + ToString( vGrav.z ) );
        texts[ texts.size() - 3 ].setString( "Gyroscope: "      + ToString( vGyro.x ) + ", " + ToString( vGyro.y ) + ", " + ToString( vGyro.z ) );



        app.clear();

        app.draw( background );
        app.draw( object );

        for ( auto& text : texts )
        {
            app.draw( text );
        }

        app.display();
    }

    return EXIT_SUCCESS;
}
