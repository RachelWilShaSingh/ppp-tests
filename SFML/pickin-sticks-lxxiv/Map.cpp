#include "Map.h"

Map::Map()
    : m_tileSize( 32, 32 )
{
}

void Map::Setup( sf::Vector2f dimensions )
{
    m_dimensions = dimensions;
    m_tileset.loadFromFile( "assets/tileset.png" );

    for ( int y = 0; y < m_dimensions.y / m_tileSize.y; y++ )
    {
        for ( int x = 0; x < m_dimensions.x / m_tileSize.x; x++ )
        {
            sf::Sprite sprite;
            sprite.setTexture( m_tileset );
            sprite.setTextureRect( sf::IntRect( 0, 0, 32, 32 ) );
            sprite.setPosition( x * m_tileSize.x, y * m_tileSize.y );
            m_map.push_back( sprite );
        }
    }
}

void Map::Draw( sf::RenderWindow& win )
{
    for ( auto& sprite : m_map )
    {
        win.draw( sprite );
    }
}
