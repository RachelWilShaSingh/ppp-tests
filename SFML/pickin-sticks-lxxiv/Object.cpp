#include "Object.h"

void Object::Setup( sf::Texture& texture, sf::Vector2f position )
{
    m_sprite.setTexture( texture );
    m_sprite.setPosition( position );
    m_sprite.setTextureRect( sf::IntRect( 0, 0, 40, 40 ) );
    m_velocity.x = 0;
    m_velocity.y = 0;
    m_maxVelocity.x = 10;
    m_maxVelocity.y = m_maxVelocity.x;
    m_deacc = 0.2;
}

void Object::Draw( sf::RenderWindow& win )
{
    win.draw( m_sprite );
}

sf::IntRect Object::GetBoundingBox()
{
    sf::IntRect boundingBox;
    boundingBox.left = m_sprite.getPosition().x;
    boundingBox.top  = m_sprite.getPosition().y;
    boundingBox.width = 40;
    boundingBox.height = 40;
    return boundingBox;
}

void Object::RandomPosition( sf::Vector2f pos )
{
    m_sprite.setPosition( pos );
}

sf::Vector2f Object::GetCenterPoint()
{
    sf::Vector2f center = m_sprite.getPosition();
    center.x += 20;
    center.y += 20;
    return center;
}

void Object::Update( sf::IntRect boundaries )
{
    sf::Vector2f pos = m_sprite.getPosition();
    if ( m_velocity.x <= -0.5 || m_velocity.x >= 0.5 )
    {
        pos.x += m_velocity.x;
    }
    if ( m_velocity.y <= -0.5 || m_velocity.y >= 0.5  )
    {
        pos.y += m_velocity.y;
    }

    if      ( pos.x < boundaries.left )
    {
        pos.x = boundaries.left;
        m_velocity.x = -m_velocity.x;
    }
    else if ( pos.x > boundaries.left + boundaries.width - 40 )
    {
        pos.x = boundaries.left + boundaries.width - 40;
        m_velocity.x = -m_velocity.x;
    }

    if      ( pos.y < boundaries.top )
    {
        pos.y = boundaries.top;
        m_velocity.y = -m_velocity.y;
    }
    else if ( pos.y > boundaries.top + boundaries.height - 40 )
    {
        pos.y = boundaries.top + boundaries.height - 40;
        m_velocity.y = -m_velocity.y;
    }

    m_sprite.setPosition( pos );
}

void Object::Accelerate( sf::Vector2f acc )
{
    m_velocity.x += acc.x;
    m_velocity.y += acc.y;

    m_velocity.x = ( m_velocity.x <= m_maxVelocity.x )  ? m_velocity.x : m_maxVelocity.x;
    m_velocity.x = ( m_velocity.x >= -m_maxVelocity.x ) ? m_velocity.x : -m_maxVelocity.x;

    m_velocity.y = ( m_velocity.y <= m_maxVelocity.y )  ? m_velocity.y : m_maxVelocity.y;
    m_velocity.y = ( m_velocity.y >= -m_maxVelocity.y ) ? m_velocity.y : -m_maxVelocity.y;
}

void Object::Deaccelerate()
{
    if      ( m_velocity.x > 0 )    { m_velocity.x += -m_deacc; }
    else if ( m_velocity.x < 0 )    { m_velocity.x += m_deacc; }
    else { m_velocity.x = 0; }

    if      ( m_velocity.y > 0 )    { m_velocity.y += -m_deacc; }
    else if ( m_velocity.y < 0 )    { m_velocity.y += m_deacc; }
    else { m_velocity.y = 0; }
}

sf::Vector2f Object::GetVelocity()
{
    return m_velocity;
}
