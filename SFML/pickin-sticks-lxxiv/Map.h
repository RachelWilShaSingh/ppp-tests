#ifndef _MAP_H
#define _MAP_H

#include <SFML/Graphics.hpp>

#include <vector>

class Map
{
public:
    Map();
    void Setup( sf::Vector2f dimensions );
    void Draw( sf::RenderWindow& win );

private:
    sf::Texture m_tileset;
    std::vector<sf::Sprite> m_map;
    sf::Vector2f m_dimensions;
    const sf::Vector2f m_tileSize;
};

#endif
