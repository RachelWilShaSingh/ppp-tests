#include "Controller.h"

#include <iostream>

Controller::Controller()
    : m_imageDimensions( 320, 150 )
{
}

void Controller::Setup( sf::Vector2f dimensions )
{
    m_txBackground.loadFromFile( "assets/hudbg.png" );
    for ( int x = 0; x < dimensions.x; x += m_imageDimensions.x )
    {
        sf::Sprite sprite;
        sprite.setTexture( m_txBackground );
        sprite.setPosition( x, dimensions.y - 150 );
        m_backgrounds.push_back( sprite );
    }

    int dpadWidthHeight = 120;
    int dpadX = dimensions.x / 2 - dpadWidthHeight / 2;
    int dpadY = dimensions.y - 150;
    m_txDpad.loadFromFile( "assets/dpad.png" );
    m_dpadbg.setTexture( m_txDpad );
    m_dpadbg.setPosition( dpadX, dpadY );

    m_txButtonSheet.loadFromFile( "assets/dpad_keys.png" );
    for ( int y = 0; y < 3; y++ )
    {
        for ( int x = 0; x < 3; x++ )
        {
            m_directions[x][y].setTexture( m_txButtonSheet );
            m_directions[x][y].setPosition( dpadX + x * 40, dpadY + y * 40 );
            m_directions[x][y].setTextureRect( sf::IntRect( x * 40, y * 40 + 120, 40, 40 ) );
        }
    }
}

sf::Vector2f Controller::Update( sf::Vector2i mousePosition )
{
    sf::Vector2f movement( 0, 0 );

    // Check to see if any buttons are being pressed
    for ( int y = 0; y < 3; y++ )
    {
        for ( int x = 0; x < 3; x++ )
        {
            sf::Vector2f pos = m_directions[x][y].getPosition();
            if ( mousePosition.x >= pos.x && mousePosition.x <= pos.x + 40 &&
                    mousePosition.y >= pos.y && mousePosition.y <= pos.y + 40 )
            {
                // Being pressed
                m_directions[x][y].setTextureRect( sf::IntRect( x * 40, y * 40, 40, 40 ) );
                movement.x = x - 1;
                movement.y = y - 1;
            }
            else
            {
                // Not being pressed
                m_directions[x][y].setTextureRect( sf::IntRect( x * 40, y * 40 + 120, 40, 40 ) );
            }
        }
    }

    return movement;
}

void Controller::Clear()
{
    for ( int y = 0; y < 3; y++ )
    {
        for ( int x = 0; x < 3; x++ )
        {
            m_directions[x][y].setTextureRect( sf::IntRect( x * 40, y * 40 + 120, 40, 40 ) );
        }
    }
}

void Controller::Draw( sf::RenderWindow& win )
{
    for ( auto& sprite : m_backgrounds )
    {
        win.draw( sprite );
    }

    win.draw( m_dpadbg );

    for ( int y = 0; y < 3; y++ )
    {
        for ( int x = 0; x < 3; x++ )
        {
            win.draw( m_directions[x][y] );
        }
    }
}
