#ifndef _OBJECT_H
#define _OBJECT_H

#include <SFML/Graphics.hpp>

class Object
{
public:
    void Setup( sf::Texture& texture, sf::Vector2f position );
    void Draw( sf::RenderWindow& win );
    void Update( sf::IntRect boundaries );
    void Accelerate( sf::Vector2f acc );
    void Deaccelerate();
    void RandomPosition( sf::Vector2f pos );
    sf::Vector2f GetVelocity();
    sf::IntRect GetBoundingBox();
    sf::Vector2f GetCenterPoint();

private:
    sf::Sprite m_sprite;
    sf::Vector2f m_velocity;
    sf::Vector2f m_maxVelocity;
    float m_deacc;
};

#endif
