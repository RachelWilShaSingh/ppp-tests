#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>

#include "../Fps.h"
#include "Map.h"
#include "Controller.h"
#include "Object.h"

template <typename T>
std::string ToString( const T& value )
{
    std::stringstream ss;
    ss << value;
    return ss.str();
}

float GetDistance( sf::Vector2f point1, sf::Vector2f point2 )
{
    float diffX = point1.x - point2.x;
    float diffY = point1.y - point2.y;
    return sqrt( diffX * diffX + diffY * diffY );
}

bool Collision( sf::IntRect obj1, sf::IntRect obj2 )
{
    sf::Vector2f center1( obj1.left + obj1.width / 2, obj1.top + obj1.height / 2 );
    sf::Vector2f center2( obj2.left + obj2.width / 2, obj2.top + obj2.height / 2 );
    return ( GetDistance( center1, center2 ) <= 40 );
}

int main()
{
    srand( time( NULL ) );

    sf::Vector2f windowDimensions = sf::Vector2f( 720 / 2, 1440 / 2 );

    std::cout << windowDimensions.x << ", " << windowDimensions.y << std::endl;

    sf::RenderWindow app( sf::VideoMode( windowDimensions.x, windowDimensions.y ), "Pickin' Sticks LXXIV", sf::Style::Fullscreen );
    app.setFramerateLimit( 60 );

    FPS fps;

    sf::IntRect screenBoundaries( 0, 0, windowDimensions.x, windowDimensions.y - 150 );
    Map gameMap;
    gameMap.Setup( sf::Vector2f( windowDimensions.x, windowDimensions.y - 150 ) );

    Controller controller;
    controller.Setup( windowDimensions );

    sf::Font fntMain;
    fntMain.loadFromFile( "../assets/mononoki-Bold.ttf" );

    sf::Font fntGame;
    fntGame.loadFromFile( "../assets/PressStart2P.ttf" );

    sf::Texture txStick;
    txStick.loadFromFile( "assets/stick_filmstrip.png" );
    sf::Texture txCat;
    txCat.loadFromFile( "assets/player_animated.png" );

    Object player;
    player.Setup( txCat, sf::Vector2f( windowDimensions.x / 2, windowDimensions.y / 2 ) );

    Object stick;
    stick.Setup( txStick, sf::Vector2f( rand() % int(windowDimensions.x), rand() % int(windowDimensions.y - 190) ) );

    std::vector<sf::Text> texts;

    sf::Text txt;
    txt.setFont( fntMain );
    txt.setCharacterSize( 10 );
    txt.setFillColor( sf::Color::White );

    int x = 5, y = windowDimensions.y - 25, inc = 10;
    txt.setPosition( x, y ); y += inc; txt.setString( "Screen dimensions: " + ToString( windowDimensions.x ) + ", " + ToString( windowDimensions.y ) ); texts.push_back( txt );
    txt.setPosition( x, y ); y += inc; txt.setString( "FPS: xyz" ); texts.push_back( txt ); x = 200, y = windowDimensions.y - 25, inc = 10;
    txt.setPosition( x, y ); y += inc; txt.setString( "Velocity:" ); texts.push_back( txt );
    txt.setPosition( x, y ); y += inc; txt.setString( "Distance:" ); texts.push_back( txt );

    int score = 0;
    sf::Text txtScore;
    txtScore.setFont( fntGame );
    txtScore.setCharacterSize( 20 );
    txtScore.setFillColor( sf::Color::White );
    txtScore.setPosition( 5, windowDimensions.y - 175 );
    txtScore.setString( "Score: " + ToString( score ) );

    while ( app.isOpen() )
    {
        fps.update();

        sf::Event event;
        while ( app.pollEvent( event ) )
        {
            if ( event.type == sf::Event::Closed )
                app.close();
        }

        sf::Vector2i localPosition = sf::Mouse::getPosition( app );
        if ( sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
        {
            sf::Vector2f movement = controller.Update( localPosition );
            player.Accelerate( movement );
            texts[0].setString( "Movement: " + ToString( movement.x ) + ", " + ToString( movement.y ) );
        }
        else
        {
            controller.Clear();
            player.Deaccelerate();
        }
        player.Update( screenBoundaries );

        if ( Collision( player.GetBoundingBox(), stick.GetBoundingBox() ) )
        {
            stick.RandomPosition( sf::Vector2f( rand() % int(windowDimensions.x), rand() % int(windowDimensions.y - 190) ) );
            score++;
            txtScore.setString( "Score: " + ToString( score ) );
        }

        texts[1].setString( "FPS: " + ToString( fps.getFPS() ) );
        texts[2].setString( "Velocity: " + ToString( player.GetVelocity().x ) + ", " + ToString( player.GetVelocity().y ) );
        texts[3].setString( "Distance: " + ToString( GetDistance( player.GetCenterPoint(), stick.GetCenterPoint() ) ) );

        app.clear();

        gameMap.Draw( app );
        controller.Draw( app );

        stick.Draw( app );
        player.Draw( app );

        for ( auto& text : texts )
        {
            app.draw( text );
        }

        app.draw( txtScore );

        app.display();
    }

    return EXIT_SUCCESS;
}
