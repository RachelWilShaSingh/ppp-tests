#ifndef _CONTROLLER_H
#define _CONTROLLER_H

#include <SFML/Graphics.hpp>

#include <vector>
#include <map>
#include <string>

class Controller
{
public:
    Controller();
    void Setup( sf::Vector2f dimensions );
    sf::Vector2f Update( sf::Vector2i mousePosition );
    void Clear();
    void Draw( sf::RenderWindow& win );

private:
    sf::Texture m_txBackground;
    sf::Texture m_txDpad;
    sf::Texture m_txButtonSheet;
    std::vector<sf::Sprite> m_backgrounds;
    sf::Sprite m_dpadbg;
    sf::Sprite m_directions[3][3];
    const sf::Vector2f m_imageDimensions;
};

#endif
