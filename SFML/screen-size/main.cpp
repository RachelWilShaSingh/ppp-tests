#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>

template <typename T>
std::string ToString( const T& value )
{
    std::stringstream ss;
    ss << value;
    return ss.str();
}

// From this gist: https://gist.github.com/kenpower/7233967
class FPS
{
public:
    FPS() : mFrame(0), mFps(0) {}
    const unsigned int getFPS() const
    {
        return mFps;
    }

private:
    unsigned int mFrame;
    unsigned int mFps;
    sf::Clock mClock;

public:
    void update()
    {
        if(mClock.getElapsedTime().asSeconds() >= 1.f)
        {
            mFps = mFrame;
            mFrame = 0;
            mClock.restart();
        }

        ++mFrame;
    }
};

int main()
{
    sf::Vector2f specifiedGameSize( 1440, 720 );

    sf::RenderWindow app( sf::VideoMode( specifiedGameSize.x, specifiedGameSize.y), "Screen test" );//, sf::Style::Fullscreen );
    app.setFramerateLimit( 60 );
    sf::Vector2f windowDimensions = sf::Vector2f( sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height );

    FPS fps;

    sf::RectangleShape specifiedGameSizeRect;
    specifiedGameSizeRect.setFillColor( sf::Color( 255, 0, 0 ) );
    specifiedGameSizeRect.setPosition( 0, 0 );
    specifiedGameSizeRect.setSize( specifiedGameSize );

    sf::RectangleShape windowDimensionsRect;
    windowDimensionsRect.setFillColor( sf::Color( 0, 0, 255 ) );
    windowDimensionsRect.setPosition( 0, 0 );
    windowDimensionsRect.setSize( windowDimensions );

    sf::Font fntMain;
    fntMain.loadFromFile( "../assets/mononoki-Bold.ttf" );

    std::vector<sf::Text> texts;
    sf::Text txt;
    txt.setFont( fntMain );
    txt.setCharacterSize( 20 );
    txt.setFillColor( sf::Color::White );

    txt.setPosition( 0, 0 );  txt.setString( "windowDimensions: " + ToString( windowDimensions.x ) + ", " + ToString( windowDimensions.y ) ); texts.push_back( txt );
    txt.setPosition( 0, 20 ); txt.setString( "specifiedGameSize: " + ToString( specifiedGameSize.x ) + ", " + ToString( specifiedGameSize.y ) ); texts.push_back( txt );
    txt.setPosition( 0, 40 ); txt.setString( "mousePosition: " ); texts.push_back( txt );

    while ( app.isOpen() )
    {
        fps.update();

        sf::Event event;
        while ( app.pollEvent( event ) )
        {
            if ( event.type == sf::Event::Closed )
                app.close();
        }

        sf::Vector2i mousePosition = sf::Mouse::getPosition( app );
        texts[2].setString( "mousePosition: " + ToString( mousePosition.x ) + ", " + ToString( mousePosition.y ) );

        app.clear();

        app.draw( windowDimensionsRect );
        app.draw( specifiedGameSizeRect );

        for ( auto& text : texts )
        {
            app.draw( text );
        }

        app.display();
    }

    return EXIT_SUCCESS;
}
